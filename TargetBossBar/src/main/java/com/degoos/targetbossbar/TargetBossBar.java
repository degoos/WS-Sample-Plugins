package com.degoos.targetbossbar;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.bar.WSBossBar;
import com.degoos.wetsponge.entity.WSEntity;
import com.degoos.wetsponge.entity.living.WSLivingEntity;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBossBarColor;
import com.degoos.wetsponge.enums.EnumBossBarOverlay;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.WSEntityDamageByEntityEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerQuitEvent;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.text.WSText;

import java.util.HashMap;
import java.util.Map;

public class TargetBossBar extends WSPlugin {

    // Create a map to store the relation between players and their BossBar
    private Map<WSPlayer, WSBossBar> bossBarMap;
    private Map<WSPlayer, WSTask> taskMap;


    @Override
    public void onEnable() {
        /* This code will execute as the plugin loads, make sure you initialize your code here */
        bossBarMap = new HashMap<>();
        taskMap = new HashMap<>();

        // Add HealthBossBar to the EventManager, that way our plugin will be able to listen to the WetSponge events
        WetSponge.getEventManager().registerListener(this, this);
    }


    @Override
    public void onDisable() {
        /* This is the code that will execute on plugin unload */

        // Get all players online during the plugin unload and launch the removeBossBar method
        WetSponge.getServer().getOnlinePlayers().forEach(this::removeBossBar);
    }


    @WSListener
    public void onDamage(WSEntityDamageByEntityEvent event) {
        /* This is the code that will execute when an entity receive damage */
        WSEntity damager = event.getDamager();
        WSEntity damaged = event.getEntity();
        if (damager instanceof WSPlayer && damaged instanceof WSLivingEntity) createBossBar((WSPlayer) damager, (WSLivingEntity) damaged);
    }


    @WSListener
    public void onQuit(WSPlayerQuitEvent event) {
        /* This is the code that will execute when a players leaves the server */
        removeBossBar(event.getPlayer());
    }


    public void createBossBar(WSPlayer player, WSLivingEntity target) {
        // We create a WSText to get the Player Name or the Entity Name/Type
        WSText name;
        if (target instanceof WSPlayer) name = target.getCustomName().orElse(WSText.of(((WSPlayer) target).getName()));
        else name = target.getCustomName().orElse(WSText.of(target.getEntityType().toString()));

		/*
            We create a BossBar using the WSBossBar.builder:
			- playeEndBossMusic = false - so the player won't hear the ender dragon music
			- color = EnumBossBarColor.GREEN - you can check all the available colors on the EnumBossBarColor
			- crateFog = false - don't create the boss fog
			- darkenSky = false - don't dark the sky
			- overlay - BossBar type: 12, 16, 20 or No bars, progress = no bars
			- percent - the progress of the bossBar "health"
			- name - the name displayed in the bossBar - we use a WSText generator to translate all & to §
			- visible - well, we want to show out bar, isn't it?

			With the "build" method we make sure that the bossbar is created with the given properties
		 */
        WSBossBar bossBar = WSBossBar.builder()
                .playEndBossMusic(false)
                .color(EnumBossBarColor.GREEN)
                .createFog(false)
                .darkenSky(false)
                .overlay(EnumBossBarOverlay.PROGRESS)
                .percent((float) target.getHealth() / 20)
                .name(WSText.builder(name).translateColors().build())
                .visible(true)
                .build();

        // Ad the current player to the "bossbar visible to these players" list and create our relation in our bossBarMap
        bossBar.addPlayer(player);
        if (bossBarMap.containsKey(player)) {
            this.removeBossBar(player);
            taskMap.get(player).cancel();
        }
        bossBarMap.put(player, bossBar);

        // Create a new Task to update or delete the bossbar. This task will execute every 5 ticks, executed 20 times (every 5 ticks * 20 times = 100 ticks*times = 5
        // seconds
        WSTask wsTask = WSTask.of((task) -> {
            if (!player.isOnline() || target.isDead() || task.getTimesExecuted() == 19) {
                this.removeBossBar(player);
                task.cancel();
            } else updateBossBar(target, bossBar);
        });
        wsTask.runTaskTimer(0, 5, 20, this);
        taskMap.put(player, wsTask);
    }


    public void removeBossBar(WSPlayer player) {
        // Remove the player from the bossbar and the bossbar from our relation map
        WSBossBar bossBar = bossBarMap.remove(player);
        if (bossBar != null) bossBar.removePlayer(player);
    }


    public void updateBossBar(WSLivingEntity target, WSBossBar bossBar) {
        // Get the current player health percentage
        float percent = (float) target.getHealth() / (float) target.getMaxHealth();
        if (percent < 0) percent = 0;
        if (percent > 1) percent = 1;
        // Change the bossbar percent to the current player health
        bossBar.setPercent(percent);

        if (percent <= 0.2) // If target's life is under 20% set the bossbar color to RED
            bossBar.setColor(EnumBossBarColor.RED);
        else if (percent <= 0.5) // If target's life is under 50% set the bossbar color to YELLOW
            bossBar.setColor(EnumBossBarColor.YELLOW);
        else // In other cases set the bossbar color to green
            bossBar.setColor(EnumBossBarColor.GREEN);
    }

}
