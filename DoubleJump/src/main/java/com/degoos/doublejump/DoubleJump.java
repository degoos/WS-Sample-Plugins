package com.degoos.doublejump;


import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.block.WSBlockChangeEvent;
import com.degoos.wetsponge.event.entity.movement.WSEntityMoveEvent;
import com.degoos.wetsponge.event.entity.player.interact.WSPlayerInteractBlockEvent;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.flowpowered.math.vector.Vector3d;

import java.io.File;

public class DoubleJump extends WSPlugin {

	private Vector3d modifier, add;
	private String permission;

	private ConfigAccessor configAccessor;


	@Override
	public void onEnable () {
		WetSponge.getEventManager().registerListener(this, this);
		configAccessor = new ConfigAccessor(new File(getDataFolder(), "config.yml"));
		configAccessor.checkNodes(new ConfigAccessor(getResource("doubleJumpConfig.yml")), true);

		double modX = configAccessor.getDouble("modifier.x", 1.6);
		double modY = configAccessor.getDouble("modifier.y", 0);
		double modZ = configAccessor.getDouble("modifier.z", 1.6);
		modifier = new Vector3d(modX, modY, modZ);

		double addX = configAccessor.getDouble("add.x", 0);
		double addY = configAccessor.getDouble("add.y", 1);
		double addZ = configAccessor.getDouble("add.z", 0);
		add = new Vector3d(addX, addY, addZ);

		permission = configAccessor.getString("permission", "doubleJump.jump");
	}


	@WSListener
	public void onMove (WSEntityMoveEvent event) {
		if (!(event.getEntity() instanceof WSPlayer)) return;
		WSPlayer player = (WSPlayer) event.getEntity();
		if (!player.hasPermission(permission)) return;
		if (player.isOnGround()) {
			player.setCanFly(true);
		}
		else if (player.isFlying()) {
			player.setFlying(false);
			player.setCanFly(false);
			player.setVelocity(player.getLocation().getFacingDirection().mul(modifier).add(add));
		}

	}
}
