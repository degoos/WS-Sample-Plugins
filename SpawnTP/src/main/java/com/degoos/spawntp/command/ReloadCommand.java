package com.degoos.spawntp.command;


import com.degoos.languages.api.LanguagesAPI;
import com.degoos.spawntp.SpawnTP;
import com.degoos.spawntp.loader.ManagerLoader;
import com.degoos.spawntp.manager.FileManager;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ReloadCommand extends WSCommand {

    public ReloadCommand() {
        super("spawnReload", "Reloads the configuration.");
    }


    @Override
    public void executeCommand(WSCommandSource wsCommandSource, String s, String[] strings) {
        if (!wsCommandSource.hasPermission("spawnTp.admin")) {
            wsCommandSource.sendMessage(LanguagesAPI.getMessage(wsCommandSource, "noPermission", true, SpawnTP.getInstance())
                                                .orElse(WSText.builder("You don't have permission to use this command.").color(EnumTextColor.RED).build()));
        } else {
            ManagerLoader.getManager(FileManager.class).load();
            wsCommandSource.sendMessage(LanguagesAPI.getMessage(wsCommandSource, "reload", true, SpawnTP.getInstance())
                                                .orElse(WSText.builder("Spawns list reloaded!").color(EnumTextColor.GREEN).build()));
        }
    }


    @Override
    public List<String> sendTab(WSCommandSource wsCommandSource, String s, String[] strings) {
        return new ArrayList<>();
    }
}
