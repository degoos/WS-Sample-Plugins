package com.degoos.spawntp.command;


import com.degoos.languages.api.LanguagesAPI;
import com.degoos.spawntp.SpawnTP;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpawnCommand extends WSCommand {

	public SpawnCommand () {
		super("spawn", "Teleports you to a random spawn location.");
	}


	@Override
	public void executeCommand (WSCommandSource wsCommandSource, String s, String[] strings) {
		if (!wsCommandSource.hasPermission("spawnTp.spawn") || !(wsCommandSource instanceof WSPlayer)) {
			wsCommandSource.sendMessage(LanguagesAPI.getMessage(wsCommandSource, "noPermission", true, SpawnTP.getInstance())
					                            .orElse(WSText.builder("You don't have permission to use this command.").color(EnumTextColor.RED).build()));
		}
		else {
			if (SpawnTP.getSpawnLocations().size() > 0)
				((WSPlayer) wsCommandSource).setLocation(SpawnTP.getSpawnLocations().get(new Random().nextInt(SpawnTP.getSpawnLocations().size())));
			else wsCommandSource.sendMessage(LanguagesAPI.getMessage(wsCommandSource, "emptySpawnList", true, SpawnTP.getInstance())
					                                 .orElse(WSText.builder("There aren't any spawn point configured.").color(EnumTextColor.RED).build()));
		}
	}


	@Override
	public List<String> sendTab (WSCommandSource wsCommandSource, String s, String[] strings) {
		return new ArrayList<>();
	}
}
