package com.degoos.spawntp.command;


import com.degoos.languages.api.LanguagesAPI;
import com.degoos.spawntp.SpawnTP;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;

public class AddSpawnCommand extends WSCommand {

	public AddSpawnCommand () {
		super("addSpawn", "Adds a new spawn point location.");
	}


	@Override
	public void executeCommand (WSCommandSource wsCommandSource, String s, String[] strings) {
		if (!wsCommandSource.hasPermission("spawnTp.admin") || !(wsCommandSource instanceof WSPlayer)) {
			wsCommandSource.sendMessage(LanguagesAPI.getMessage(wsCommandSource, "noPermission", true, SpawnTP.getInstance())
					                            .orElse(WSText.builder("You don't have permission to use this command.").color(EnumTextColor.RED).build()));
		}
		else {
			WSPlayer player = (WSPlayer) wsCommandSource;
			SpawnTP.addSpawnLocation(player.getLocation(), true);
			wsCommandSource.sendMessage(LanguagesAPI.getMessage(wsCommandSource, "newLocation", true, SpawnTP.getInstance())
					                            .orElse(WSText.builder("New location added to SpawnTP.").color(EnumTextColor.GREEN).build()));
		}
	}


	@Override
	public List<String> sendTab (WSCommandSource wsCommandSource, String s, String[] strings) {
		return new ArrayList<>();
	}
}
