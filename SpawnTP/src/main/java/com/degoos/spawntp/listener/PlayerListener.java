package com.degoos.spawntp.listener;


import com.degoos.spawntp.SpawnTP;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerJoinEvent;

import java.util.Random;

public class PlayerListener {

    @WSListener
    public void onPlayerJoin(WSPlayerJoinEvent event) {
        if (SpawnTP.getSpawnLocations().size() > 0)
            event.getPlayer().setLocation(SpawnTP.getSpawnLocations().get(new Random().nextInt(SpawnTP.getSpawnLocations().size())));
    }

}
