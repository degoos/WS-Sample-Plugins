package com.degoos.spawntp.manager;


import com.degoos.spawntp.SpawnTP;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.world.WSLocation;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public class FileManager implements Manager {
    private ConfigAccessor configuration;


    public void load() {
        File pluginFolder = SpawnTP.getInstance().getDataFolder();

        configuration = new ConfigAccessor(new File(pluginFolder, "config.yml"));
        this.poblateSpawnsList();
    }

    public ConfigAccessor getConfiguration() {
        return configuration;
    }


    public void poblateSpawnsList() {
        SpawnTP.getSpawnLocations().clear();
        this.getConfiguration().getStringList("spawns_list").forEach(sp -> SpawnTP.addSpawnLocation(WSLocation.of(sp), false));
    }


    public void saveSpawnsList(List<WSLocation> spawnLocations) {
        this.getConfiguration().set("spawns_list", spawnLocations.stream().map(WSLocation::toString).collect(Collectors.toList()));
        this.getConfiguration().save();
    }

}
