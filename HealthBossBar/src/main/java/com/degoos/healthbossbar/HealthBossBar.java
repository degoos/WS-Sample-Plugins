package com.degoos.healthbossbar;


import com.degoos.languages.api.LanguagesAPI;
import com.degoos.languages.event.LanguageChangeEvent;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.bar.WSBossBar;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumBossBarColor;
import com.degoos.wetsponge.enums.EnumBossBarOverlay;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerJoinEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerQuitEvent;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.text.WSText;

import java.util.HashMap;
import java.util.Map;

public class HealthBossBar extends WSPlugin {

	// Create a map to store the relation between players and their BossBar
	private Map<WSPlayer, WSBossBar> bossBarMap;


	@Override
	public void onEnable () {
		/* This code will execute as the plugin loads, make sure you initialize your code here */
		bossBarMap = new HashMap<>();

		//Loads LanguageAPI's plugin support.
		LanguagesAPI.addPlugin(this);

		// Add HealthBossBar to the EventManager, that way our plugin will be able to listen to the WetSponge events
		WetSponge.getEventManager().registerListener(this, this);

		// Get all players online during the plugin load and launch the createBossBar method
		WetSponge.getServer().getOnlinePlayers().forEach(this::createBossBar);
	}


	@Override
	public void onDisable () {
		/* This is the code that will execute on plugin unload */

		// Get all players online during the plugin unload and launch the removeBossBar method
		WetSponge.getServer().getOnlinePlayers().forEach(this::removeBossBar);
	}


	@WSListener
	public void onJoin (WSPlayerJoinEvent event) {
		/* This is the code that will execute when a players joins the server */
		createBossBar(event.getPlayer());
	}


	@WSListener
	public void onQuit (WSPlayerQuitEvent event) {
		/* This is the code that will execute when a players leaves the server */
		removeBossBar(event.getPlayer());
	}


	public void createBossBar (WSPlayer player) {
		// We create a WSText and use LanguagesAPI to get the translation for the node "bossBarName"
		WSText name = LanguagesAPI.getMessage(player, "bossBarName", false, this).orElse(WSText.empty());

		/*
			We create a BossBar using the WSBossBar.builder:
			- playeEndBossMusic = false - so the player won't hear the ender dragon music
			- color = EnumBossBarColor.GREEN - you can check all the available colors on the EnumBossBarColor
			- crateFog = false - don't create the boss fog
			- darkenSky = false - don't dark the sky
			- overlay - BossBar type: 12, 16, 20 or No bars, progress = no bars
			- percent - the progress of the bossBar "health"
			- name - the name displayed in the bossBar - we use a WSText generator to translate all & to §
			- visible - well, we want to show out bar, isn't it?

			With the "build" method we make sure that the bossbar is created with the given properties
		 */
		WSBossBar bossBar = WSBossBar.builder()
				.playEndBossMusic(false)
				.color(EnumBossBarColor.GREEN)
				.createFog(false)
				.darkenSky(false)
				.overlay(EnumBossBarOverlay.PROGRESS)
				.percent((float) player.getHealth() / 20)
				.name(WSText.builder(name).translateColors().build())
				.visible(true)
				.build();

		// Ad the current player to the "bossbar visible to these players" list and create our relation in our bossBarMap
		bossBar.addPlayer(player);
		bossBarMap.put(player, bossBar);

		// Create a new Task to update or delete the bossbar. This task will execute every 5 ticks
		WSTask.of((task) -> {
			if (!player.isOnline()) task.cancel();
			else updateBossBar(player, bossBar);
		}).runTaskTimer(0, 5, this);
	}


	public void removeBossBar (WSPlayer player) {
		// Remove the player from the bossbar and the bossbar from our relation map
		bossBarMap.remove(player).removePlayer(player);
	}


	public void updateBossBar (WSPlayer player, WSBossBar bossBar) {
		// Get the current player health percentage
		float percent = (float) player.getHealth() / (float) player.getMaxHealth();
		// Change the bossbar percent to the current player health
		bossBar.setPercent(percent);

		if (percent <= 0.2) // If players life is under 20% set the bossbar color to RED
			bossBar.setColor(EnumBossBarColor.RED);
		else if (percent <= 0.5) // If players life is under 50% set the bossbar color to YELLOW
			bossBar.setColor(EnumBossBarColor.YELLOW);
		else // In other cases set the bossbar color to green
			bossBar.setColor(EnumBossBarColor.GREEN);
	}


	@WSListener
	public void onLanguageChange (LanguageChangeEvent event) {
		// Check if there is a language change event for a player and, in that case, change the BossBar name to its new translation
		WSBossBar bossBar = bossBarMap.get(event.getPlayer().getPlayer());
		if (bossBar == null) return;
		bossBar.setName(LanguagesAPI.getMessage(event.getPlayer().getPlayer(), "bossBarName", false, this).orElse(WSText.empty()));
	}

}
