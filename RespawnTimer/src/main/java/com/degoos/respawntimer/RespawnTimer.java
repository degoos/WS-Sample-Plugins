package com.degoos.respawntimer;


import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumGameMode;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.WSEntityDamageEvent;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.WSTitle;
import com.degoos.wetsponge.world.WSLocation;

import java.io.File;
import java.util.List;
import java.util.Random;

public class RespawnTimer extends WSPlugin {

	private ConfigAccessor configAccessor;


	@Override
	public void onEnable () {
	    /* This code will execute as the plugin loads, make sure you initialize your code here */

		// Add RespawnTimer to the EventManager, that way our plugin will be able to listen to the WetSponge events
		WetSponge.getEventManager().registerListener(this, this);
		configAccessor = new ConfigAccessor(new File(getDataFolder(), "config.yml"));
		configAccessor.checkNodes(new ConfigAccessor(getResource("respawnTimerConfig.yml")), true);
	}


	@WSListener
	public void onDeath (WSEntityDamageEvent event) {
		// if the damaged entity is not a player or won't kill the entity, don't do anything
		if (event.getEntity() instanceof WSPlayer && !event.willCauseDeath()) return;
		event.setCancelled(true);

		WSPlayer player = (WSPlayer) event.getEntity();

		// Max the player health and set his gamemode to spectator (saving his last gamemode)
		player.setHealth(player.getMaxHealth());
		EnumGameMode oldGM = player.getGameMode();
		player.setGameMode(EnumGameMode.SPECTATOR);

		// Create a new task: task.getTimesExecuted() = # of times the task has been executed
		WSTask wsTask = WSTask.of((task) -> {
			long timesLeft = configAccessor.getLong("respawnDelay") - task.getTimesExecuted();
			// If the task has not been executed "respawnDelay" times send the title
			if (timesLeft > 0) {
				// Get the title and subtitle to send from the Language
				WSText title    = LanguagesAPI.getMessage(player, "respawnTitle", false, this, "<SECONDS>", timesLeft).orElse(WSText.empty());
				WSText subtitle = LanguagesAPI.getMessage(player, "respawnSubtitle", false, this, "<SECONDS>", timesLeft).orElse(WSText.empty());

				// Create a new WSTitle and send it to the player
				WSTitle message = new WSTitle(title, subtitle, null, 5, 10, 5, false, false);
				player.sendTitle(message);
			}
			else {
				// Set the player gamemode to the last one he got
				player.setGameMode(oldGM);

				// Check if the config has SpawnTP hook enabled and also check if the plugin is enabled
				if (configAccessor.getBoolean("hookToSpawnTP") && WetSponge.getPluginManager().isPluginEnabled("SpawnTP")) {
					// Get SpawnTP locations and send the player to a random location (or config location if empty)
					List<WSLocation> locations = com.degoos.spawntp.SpawnTP.getSpawnLocations();
					if (locations.isEmpty()) {
						teleportFromConfig(player);
						return;
					}
					player.setLocation(locations.get(new Random().nextInt(locations.size())));
				}
				else {
					// Teleport the player to the config location
					teleportFromConfig(player);
				}
				task.cancel();
			}
		});
		wsTask.runTaskTimer(0, 20, configAccessor.getLong("respawnDelay"), this);
	}


	public void teleportFromConfig (WSPlayer player) {
		try {
			player.setLocation(WSLocation.of(configAccessor.getString("respawnLocation")));
		} catch (Exception ex) {
			LanguagesAPI.sendMessage(player, "wrongRespawnLocation", this);
		}
	}
}
